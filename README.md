# RL_DDPG_HalfCheetah

This project contains a pdf description of the DDPG Reinforcement Learning algorithm and a notebook containing the code implementation. 
The results obtained from its application are shown on the HalfCheetah-v2 environment of Mujoco.

![Alt text](Trained_2.gif)[](Trained_2.gif)
_Trained Half Cheetah, total score 3873_

![Alt text](Untrained_2.gif)[](Untrained_2.gif)
_Untrained Half Cheetah_

**Introduction**

DDPG stands for ‘Deep Deterministic Policy Gradient’. It is a Reinforcement Learning off-policy algorithm that presents an actor-critic, model free algorithm based on the deterministic policy gradient for continuous action domains.

**Algorithm**

DDPG currently learns a Q-function and a policy. 
For this aim, DDPG uses the Actor-Critic architecture, composed of two neural networks: the Actor and the Critic.
The Critic Q is a Q-value network that takes a state s and an action a as input and outputs the Q-value for the state-action pair.
The Actor μ, is a deterministic policy network, that uses the Q-function to learn the policy μ(s) that gives the action that maximizes Q(s,a).
Other details and mathematical formulas are discussed in the pdf attached to the repo. 


 ![](actor_c.png)


**Replay Buffer**

DDPG uses a Replay Buffer to sample previous experiences, later used to update neural network parameters.
The replay buffer has been implemented as a list of tuples, a finite-sized cache. In fact during the training, every time an action is performed by the agent, the response of the environment is stored in the buffer as a tuple (state, action, reward, next state, done). To update the value and the policy networks, mini-batches of experience are randomly sampled from the buffer.
It should be noted that storing the data in a replay buffer and taking random batches satisfies the request of having the data to be independently distributed.

**Exploration**

Since the action space is continuous, exploration in DDPG is done by adding, at training time, noise sampled from a noise process N to the action determined by the policy μ, where the noise process can be chosen to suit the environment.

 ![](CodeCogsEqn.gif)

In the experiments different types of noise (OU, Gaussian, Adaptive Gaussian) with different parameter settings have been considered and the results obtained compared.

Moreover, to improve the exploration at the beginning of the training, a number of initial random steps has been used. Therefore the agent, for the first few episodes, takes random actions from the environment.

**Experiments**

The environment taken into account is Halfcheetah-v2 of Mujoco. The agent performs only continuous actions. It moves a 2D bipedal body, made of 8 articulated rigid links. The code can be easily adapted to different gym environments with continuos action spaces. 
The experiments conducted are related to the networks structure, noise considered, buffer implementation e parameters settings. 

